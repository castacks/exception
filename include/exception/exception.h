/******************************************************************************
 * Filename: exception.h
 * Description:
 *
 * Version: 1.0
 * Created: Nov 06 2014 15:50:21
 * Last modified: Mar 25 2015 01:10:16 PM
 * Revision: None
 *
 * Author:  althoff
 * e-mail:  althoff@andrew.cmu.edu
 *
 * Revision history
 * Date Author Remarks
 * Nov 06 2014 althoff File created.
 *
 ******************************************************************************/
#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

namespace ca{
class Exception : public std::exception {
	public: 
		Exception(
			const std::string &msg, 
			const std::string &file,
			unsigned int line,
			const std::string &function) : msg_(format(msg,file,line,function)), err_(msg) {};
		static std::string format(
			const std::string &msg,
			const std::string &file,
			unsigned int line,
			const std::string &function) {
			
			std::string message("\n### Internal CMU error");
			message+="\n### file " + file + ", line " + std::to_string(line) + ", function " + function;
			message+="\n### message: " + msg + "\n";
			return message;
		};
		const char* err() const { return err_.c_str(); }
		virtual const char* what() const throw() {return msg_.c_str();}
		/*members*/
		std::string msg_;
        std::string err_;
};
#define CA_ERROR(msg) throw ca::Exception(msg,__FILE__,__LINE__,__PRETTY_FUNCTION__)
}//namespace ca
#endif

